
/**
 * Function to change remove "Search this site..." on focus/click
 */
$(document).ready(function() {
  $('input.search-box').focus((function() {    
      if (this.value == this.defaultValue){  
          this.value = '';  
      }  
      if(this.value != this.defaultValue){  
          this.select();  
      }  
   }));  
});