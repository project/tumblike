<?php

/**
  * Intercept page template variables
  *
  * @param $vars
  *   A sequential array of variables passed to the theme function.
  *   
  *   In this case used to add a class to body if search_box enabled
  */
function tumblike_preprocess_page(&$vars) {
  if($vars[search_box]){
    $vars['body_classes'] .= ' ' . ('search');
  }
}


/**
  * Intercept node template variables
  *
  * @param $vars
  *   A sequential array of variables passed to the theme function.
  *   
  */
function tumblike_preprocess_node(&$vars) {
  // Grab the node object.
  $node = $vars['node'];
  // Make individual variables for the parts of the date.
  $vars['date_day'] = format_date($node->created, 'custom', 'j');
  $vars['date_month'] = format_date($node->created, 'custom', 'F');
  $vars['date_year'] = format_date($node->created, 'custom', 'Y');
}


/**
  * Alter theme_links
  *
	* @param $vars
	* A sequential array of variables passed to the theme function.
	*   
	* 
  */
function tumblike_links($links) { 
  
  //reorder links before passing them to default link theme function
  $searchkeys = array('node_read_more', 'comment_comments');
  $searchkeys = array_reverse($searchkeys);
  foreach ($searchkeys as $key) {
      if( isset( $links[$key] ) ) {
            $temparray = array();
            $temparray[$key] = $links[$key];
            unset( $links[$key] );
            $links = array_merge($temparray, $links);
            unset( $temparray );
      }
  }
  //Remove " * new comment" link
  unset($links['comment_new_comments']);
  
  //Change the text of comment add link to "add comment"
  if ($links['comment_add'] !='') {
    $links['comment_add']['title'] = t('Add comment');
  }
  return theme_links($links);
}
/**
* Override or insert variables into the search_theme_form template.
*
* @param $vars
*   A sequential array of variables to pass to the theme template.
*/
function tumblike_preprocess_search_theme_form(&$vars) {

  // Modify elements of the search form
  $vars['form']['search_theme_form']['#title'] = t('');
 
  // Set a default value for the search box
  $vars['form']['search_theme_form']['#value'] = t('Search this Site...');
 
  // Add a custom class and placeholder text to the search box
  $vars['form']['search_theme_form']['#attributes'] = array('class' => 'search-box');


  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_theme_form']['#printed']);
  $vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);


  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}