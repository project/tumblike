<?php
?>
<div id="node<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">

<?php print $picture ?>

<?php if (!$page): ?>
  <h3><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h3>
<?php endif; ?>


  <?php if ($terms): ?>
    <div class="terms terms-inline"><?php print $terms ?></div>
  <?php endif;?>


  <div class="content node-content">
  <?php if (!empty($date_month) || ($date_day) || ($date_year)): ?>
  <div class="submitted">  
    <span class="month"><?php print $date_month ?></span> &rarr;<span class="date"><?php print ' ' . $date_day; ?></span> &rarr;<span class="year"><?php print ' ' . $date_year; ?></span> 
  </div>
  <?php endif; ?>  
  <?php print $content ?>
  </div>
  <?php if(!empty($node->links)): ?>
    <div class="node-links">
	   <?php print $links; ?>	
	  </div>
  <?php endif; ?>
</div>