<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>  
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $body_classes; ?>">

<div id="container">
  <div id="header">
	  <?php if (!empty($site_name)): ?>
	    <h1>
	      <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
	    </h1>
	  <?php endif; ?>
	  
	  <?php if (!empty($site_slogan)): ?>
	    <div id="site-slogan"><?php print $site_slogan; ?></div>
	  <?php endif; ?>
	  <!-- /name-and-slogan -->
	  <!-- /logo-title -->
	
	      
	  <?php if (!empty($header)): ?>
	  <div id="header-region">
	    <?php print $header; ?>
	  </div>
	  <?php endif; ?>
	  <!-- /header -->
	
	  <div id="navigation" class="menu <?php if (!empty($primary_links)) { print "withprimary"; } if (!empty($secondary_links)) { print " withsecondary"; } ?> ">
	  <?php if (!empty($primary_links)): ?>
	    <div id="primary" class="clear-block">
	      <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
	    </div>
	  <?php endif; ?>
	  </div> <!-- /navigation -->
  </div><!--  /header -->
  
  <div id="aside"> 
    <?php print $search_box; ?>
  </div><!-- /search -->

    
  <div id="main-content">     
    <?php if (!empty($before_content)): ?><div id="before-content"><?php print $before_content; ?></div><?php endif; ?>
    <?php if (!empty($title)): ?><h2><?php print $title; ?></h2><?php endif; ?>
    <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
    <?php if (!empty($messages)): print $messages; endif; ?>
    <?php if (!empty($help)): print $help; endif; ?>
    <?php print $content; ?>
    <!-- /content -->
    <?php print $feed_icons; ?>
    <!-- /content -->
  </div><!-- /main-content -->

  <div id="footer"class="clear">
  <?php print $footer_message; ?>
  <?php if (!empty($footer)): print $footer; endif; ?>
  <!-- /footer -->
  <!-- /footer-wrapper -->
  </div>
  <?php print $closure; ?>

</div>  <!-- /container -->

</body>
</html>
